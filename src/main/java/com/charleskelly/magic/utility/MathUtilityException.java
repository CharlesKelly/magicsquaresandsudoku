package com.charleskelly.magic.utility;

/**
 * Created by Charles on 9/26/2016.
 */
public class MathUtilityException extends Exception
{
    public MathUtilityException (String message)
    {
        super(message);
    }
}
