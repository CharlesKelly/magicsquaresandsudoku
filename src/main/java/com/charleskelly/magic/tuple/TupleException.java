package com.charleskelly.magic.tuple;

/**
 * Created by Charles on 9/26/2016.
 */
public class TupleException extends Exception
{
    public TupleException (String message)
    {
        super(message);
    }
}
