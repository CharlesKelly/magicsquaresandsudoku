This repository contains Java code for building Magic Squares and Sudoku puzzles.

The code is used in the LinkedIn Learning (Lynda.com) course: Java for Data Scientists Essential Training.

The code enables you to create Magic Hyper Cubes with an arbitrarily large size, and an arbitrarily large number of dimensions.

The code enables you to create Sudoku puzzles that are larger than the "9x9" puzzles frequently seen in newspapers and Sudoku books.

The algorithms use four dimensional magic hyper cubes to create two dimensional Sudoku puzzles.

You can find more information about the algorithms used within the code at: https://plus.maths.org/content/maths-magic-squares-0

You can download free Android games that use the code at: https://play.google.com/store/apps/details?id=com.charleskelly.sudoku&hl=en